package db;
/**
 *
 * @author Pawel Glowacz i Michal Wojcik
 */
import java.sql.*;
import javax.swing.*;
/** Klasa odpowiedzialna za połączenie z bazą danych  */
public class ConnectionClass {

    Connection conn = null;
    
/** Inicjalizacja sterownika oraz zdefiniowanie parametrów połączenia 
     * @return  */
    public static Connection ConnecrDb() {
        try {

            Class.forName("oracle.jdbc.driver.OracleDriver"); // inicjalizacja sterownika oracle       
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "lombard", "lombard");

            return conn;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;

        }
    }

}
