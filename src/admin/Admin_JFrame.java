package admin;


/**
 *
 * @author Pawel Glowacz i Michal Wojcik
 */
import admin.About_us;
import admin.Login_Frame;
import db.ConnectionClass;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.sql.*;
import java.text.MessageFormat;
import javax.swing.*;
import java.util.*;
import net.proteanit.sql.DbUtils;

/**
 * Tworzy nowe okno Admin_JFrame
 */
public class Admin_JFrame extends javax.swing.JFrame {

    Connection conn = null;
    ResultSet rs = null;
    PreparedStatement pst = null;

    /**
     * Inicjalizacja komponentów okna Admin_JFrame
     */
    public Admin_JFrame() {
        initComponents();
        conn = ConnectionClass.ConnecrDb();
        CurrentDate();
        UpdateJTable();
    }
   /**
     * Zamyka okno
     */
    public void close3() {
        WindowEvent winClosingEvent = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);
    }
   /**
     * Aktualizujemy tabele
     */
    private void UpdateJTable() {

        String sql = "Select  id,name as imie,surname as nazwisko,userid as login,password as haslo,case access_level when 0 then 'Admin' WHEN 1 THEN 'Moderator' else 'Klient' END as Rodzaj  from USERS ";

        try {
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            jTable1_pumpdata.setModel(DbUtils.resultSetToTableModel(rs)); 

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                rs.close();
                pst.close();
            } catch (Exception e) {
            }
        }

    }
   /**
     *  Aktualna data
     */
    public void CurrentDate() {
        Calendar cal = new GregorianCalendar();
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        jMenu3.setText("Data : " + day + "/" + (month + 1) + "/" + year);
        int second = cal.get(Calendar.SECOND);
        int minute = cal.get(Calendar.MINUTE);
        int hour = cal.get(Calendar.HOUR);
        Lbl_time.setText("Godzina  " + hour + " : " + minute + " : " + second);
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        cmd_SignOut = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_employeeid = new javax.swing.JTextField();
        txt_name = new javax.swing.JTextField();
        txt_surname = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txt_userid = new javax.swing.JTextField();
        txt_password = new javax.swing.JTextField();
        txt_access = new javax.swing.JComboBox();
        jPanel3 = new javax.swing.JPanel();
        txt_search = new javax.swing.JTextField();
        cmd_search = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        cmd_add = new javax.swing.JButton();
        cmd_edit = new javax.swing.JButton();
        cmd_deleta = new javax.swing.JButton();
        cmd_clear = new javax.swing.JButton();
        cmd_refresh = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1_pumpdata = new javax.swing.JTable();
        jToolBar2 = new javax.swing.JToolBar();
        jButton2 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenu3 = new javax.swing.JMenu();
        Lbl_time = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Admin Page");

        jToolBar1.setBackground(new java.awt.Color(204, 255, 204));
        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        cmd_SignOut.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cmd_SignOut.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/SignOut-icon.png"))); // NOI18N
        cmd_SignOut.setText("Wyloguj się");
        cmd_SignOut.setFocusable(false);
        cmd_SignOut.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        cmd_SignOut.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        cmd_SignOut.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        cmd_SignOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmd_SignOutActionPerformed(evt);
            }
        });
        jToolBar1.add(cmd_SignOut);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Informacje o użytkowniku", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(0, 51, 204))); // NOI18N

        jLabel1.setText("ID");

        jLabel2.setText("Imie");

        jLabel3.setText("Nazwisko");

        jLabel4.setText("Poziom dostępu");

        txt_employeeid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_employeeidActionPerformed(evt);
            }
        });

        txt_name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nameActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel9.setText("Login");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel10.setText("Hasło");

        txt_access.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Administrator", "Moderator", "Klient" }));
        txt_access.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_accessActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_password)
                    .addComponent(txt_access, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_userid)
                            .addComponent(txt_name)
                            .addComponent(txt_employeeid, javax.swing.GroupLayout.DEFAULT_SIZE, 117, Short.MAX_VALUE)
                            .addComponent(txt_surname))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(36, 36, 36))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txt_employeeid, txt_name, txt_surname, txt_userid});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel10, jLabel9});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txt_employeeid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txt_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_surname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(txt_access, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_userid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Szukaj"));

        txt_search.setText("search......");
        txt_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_searchActionPerformed(evt);
            }
        });
        txt_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_searchKeyReleased(evt);
            }
        });

        cmd_search.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/search.png"))); // NOI18N
        cmd_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmd_searchActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Komendy", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Agency FB", 1, 15), new java.awt.Color(0, 0, 153))); // NOI18N

        cmd_add.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Button-Add-icon.png"))); // NOI18N
        cmd_add.setText("  Dodaj");
        cmd_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmd_addActionPerformed(evt);
            }
        });

        cmd_edit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Actions-edit-clear-icon (1).png"))); // NOI18N
        cmd_edit.setText("Edytuj");
        cmd_edit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmd_editActionPerformed(evt);
            }
        });

        cmd_deleta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bin-2-icon.png"))); // NOI18N
        cmd_deleta.setText("Usuń");
        cmd_deleta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmd_deletaActionPerformed(evt);
            }
        });

        cmd_clear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/eraser (1).png"))); // NOI18N
        cmd_clear.setText("Wyczyść");
        cmd_clear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmd_clearActionPerformed(evt);
            }
        });

        cmd_refresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Button-Refresh-icon (1).png"))); // NOI18N
        cmd_refresh.setText("Odśwież");
        cmd_refresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmd_refreshActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmd_edit, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmd_add, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmd_deleta, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmd_clear, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmd_refresh, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cmd_add)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmd_edit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmd_deleta)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmd_clear)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmd_refresh)
                .addContainerGap(65, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(txt_search, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmd_search, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txt_search)
                    .addComponent(cmd_search, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTable1_pumpdata.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "ID", "Imie", "Nazwisko", "Title 4", "Title 5", "Title 6"
            }
        ));
        jTable1_pumpdata.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        jTable1_pumpdata.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1_pumpdataMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1_pumpdata);

        jToolBar2.setBackground(new java.awt.Color(204, 255, 204));
        jToolBar2.setFloatable(false);
        jToolBar2.setRollover(true);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/left-arrow.png"))); // NOI18N
        jButton2.setText("Powrót");
        jButton2.setFocusable(false);
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar2.add(jButton2);

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        jMenu4.setText("Help");

        jMenuItem1.setText("About us");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem1);
        jMenu4.add(jSeparator1);

        jMenuBar1.add(jMenu4);

        jMenu3.setForeground(new java.awt.Color(204, 0, 153));
        jMenu3.setText("Date");
        jMenu3.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jMenuBar1.add(jMenu3);

        Lbl_time.setForeground(new java.awt.Color(204, 0, 153));
        Lbl_time.setText("Time");
        Lbl_time.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jMenuBar1.add(Lbl_time);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 784, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, 674, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, 0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmd_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmd_searchActionPerformed

        try {
            String sql = "select * from USERS where ID=? ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_search.getText());

            rs = pst.executeQuery();
            // pst.execute();
            if (rs.next()) {
                String add1 = rs.getString("ID");
                txt_employeeid.setText(add1);
                String add2 = rs.getString("Surname");
                txt_surname.setText(add2);
                String add3 = rs.getString("Name");
                txt_name.setText(add3);
                String add4temp = rs.getString("Access_level");

                Integer add4 = 0;
                if ("Admin".equals(add4temp)) {
                    add4 = 0;
                } else if ("Moderator".equals(add4temp)) {
                    add4 = 1;

                } else {
                    add4 = 2;
                }

                txt_access.setSelectedIndex(add4);

                String add9 = rs.getString("Userid");
                txt_userid.setText(add9);
                String add10 = rs.getString("password");
                txt_password.setText(add10);

            }
        } catch (Exception e) {
            //   JOptionPane.showMessageDialog(null, e);

        } finally {
            try {
                rs.close();
                pst.close();
                //conn.close();
            } catch (Exception e) {
            }
        }

        try {
            String sql = "select * from USERS where Name=? ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_search.getText());

            rs = pst.executeQuery();
            // pst.execute();
            if (rs.next()) {
                String add1 = rs.getString("ID");
                txt_employeeid.setText(add1);
                String add2 = rs.getString("Surname");
                txt_surname.setText(add2);
                String add3 = rs.getString("Name");
                txt_name.setText(add3);
                String add4temp = rs.getString("Access_level");

                Integer add4 = 0;
                if ("Admin".equals(add4temp)) {
                    add4 = 0;
                } else if ("Moderator".equals(add4temp)) {
                    add4 = 1;

                } else {
                    add4 = 2;
                }
                txt_access.setSelectedIndex(add4);

                String add9 = rs.getString("Userid");
                txt_userid.setText(add9);
                String add10 = rs.getString("password");
                txt_password.setText(add10);

            }
        } catch (Exception e) {
            // JOptionPane.showMessageDialog(null, e);

        } finally {
            try {
                rs.close();
                pst.close();
                // conn.close();
            } catch (Exception e) {
            }
        }

        try {
            String sql = "select * from USERS where Surname=? ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_search.getText());

            rs = pst.executeQuery();
            // pst.execute();
            if (rs.next()) {
                String add1 = rs.getString("ID");
                txt_employeeid.setText(add1);
                String add2 = rs.getString("Surname");
                txt_surname.setText(add2);
                String add3 = rs.getString("Name");
                txt_name.setText(add3);
                String add4temp = rs.getString("Access_level");

                Integer add4 = 0;
                if ("Admin".equals(add4temp)) {
                    add4 = 0;
                } else if ("Moderator".equals(add4temp)) {
                    add4 = 1;

                } else {
                    add4 = 2;
                }
                txt_access.setSelectedIndex(add4);

                String add9 = rs.getString("Userid");
                txt_userid.setText(add9);
                String add10 = rs.getString("password");
                txt_password.setText(add10);

            }
        } catch (Exception e) {
            // JOptionPane.showMessageDialog(null, e);

        } finally {
            try {
                rs.close();
                pst.close();
                //conn.close();
            } catch (Exception e) {
            }
        }

        try {
            String sql = "select * from USERS where USERID=? ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_search.getText());

            rs = pst.executeQuery();
            // pst.execute();
            if (rs.next()) {
                String add1 = rs.getString("ID");
                txt_employeeid.setText(add1);
                String add2 = rs.getString("Surname");
                txt_surname.setText(add2);
                String add3 = rs.getString("Name");
                txt_name.setText(add3);
                String add4temp = rs.getString("Access_level");

                Integer add4 = 0;
                if ("Admin".equals(add4temp)) {
                    add4 = 0;
                } else if ("Moderator".equals(add4temp)) {
                    add4 = 1;

                } else {
                    add4 = 2;
                }
                txt_access.setSelectedIndex(add4);

                String add9 = rs.getString("Userid");
                txt_userid.setText(add9);
                String add10 = rs.getString("password");
                txt_password.setText(add10);

            }
        } catch (Exception e) {
            // JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                rs.close();
                pst.close();
                //conn.close();
            } catch (Exception e) {
            }
        }


    }//GEN-LAST:event_cmd_searchActionPerformed

    private void txt_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_searchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_searchActionPerformed

    private void txt_searchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_searchKeyReleased

        // Ustawiamy kolor wyszukiwarki
        if (txt_search.getText().isEmpty()) {
            cmd_search.setBackground(Color.red);
        } else {
            cmd_search.setBackground(Color.GRAY);
        }

        try {
            // Wyszukiwarka po ID
            String sql = "select * from USERS where ID=? ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_search.getText());

            rs = pst.executeQuery();
            if (rs.next()) {
                String add1 = rs.getString("ID");
                txt_employeeid.setText(add1);
                String add2 = rs.getString("Surname");
                txt_surname.setText(add2);
                String add3 = rs.getString("Name");
                txt_name.setText(add3);
                
                //zmienna pomocnicza - Sprawdzamy poziom dostępu i ustawiamy index w liście rozwijanej
                String add4temp = rs.getString("Access_level");
                Integer add4 = 0;   
                if ("Admin".equals(add4temp)) {
                    add4 = 0;
                } else if ("Moderator".equals(add4temp)) {
                    add4 = 1;

                } else {
                    add4 = 2;
                }
                
                txt_access.setSelectedIndex(add4);

                String add9 = rs.getString("Userid");
                txt_userid.setText(add9);
                String add10 = rs.getString("password");
                txt_password.setText(add10);

            }
        } catch (Exception e) {

        } finally {
            try {
                rs.close();
                pst.close();
            } catch (Exception e) {
            }
        }

        try {
            // Wyszukiwarka po Imieniu
            String sql = "select * from USERS where Name=? ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_search.getText());

            rs = pst.executeQuery();
            if (rs.next()) {
                String add1 = rs.getString("ID");
                txt_employeeid.setText(add1);
                String add2 = rs.getString("Surname");
                txt_surname.setText(add2);
                String add3 = rs.getString("Name");
                txt_name.setText(add3);
                
                //zmienna pomocnicza - Sprawdzamy poziom dostępu i ustawiamy index w liście rozwijanej
                String add4temp = rs.getString("Access_level");
                Integer add4 = 0;
                if ("Admin".equals(add4temp)) {
                    add4 = 0;
                } else if ("Moderator".equals(add4temp)) {
                    add4 = 1;

                } else {
                    add4 = 2;
                }
                txt_access.setSelectedIndex(add4);

                String add9 = rs.getString("Userid");
                txt_userid.setText(add9);
                String add10 = rs.getString("password");
                txt_password.setText(add10);

            }
        } catch (Exception e) {

        } finally {
            try {
                rs.close();
                pst.close();
                //conn.close();
            } catch (Exception e) {
            }
        }

        try {
            //Wyszukiwarka po nazwisku
            String sql = "select * from USERS where Surname=? ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_search.getText());

            rs = pst.executeQuery();
            if (rs.next()) {
                String add1 = rs.getString("ID");
                txt_employeeid.setText(add1);
                String add2 = rs.getString("Surname");
                txt_surname.setText(add2);
                String add3 = rs.getString("Name");
                txt_name.setText(add3);
                
                //zmienna pomocnicza - Sprawdzamy poziom dostępu i ustawiamy index w liście rozwijanej
                String add4temp = rs.getString("Access_level");
                Integer add4 = 0;
                if ("Admin".equals(add4temp)) {
                    add4 = 0;
                } else if ("Moderator".equals(add4temp)) {
                    add4 = 1;

                } else {
                    add4 = 2;
                }
                txt_access.setSelectedIndex(add4);

                String add9 = rs.getString("Userid");
                txt_userid.setText(add9);
                String add10 = rs.getString("password");
                txt_password.setText(add10);

            }
        } catch (Exception e) {

        } finally {
            try {
                rs.close();
                pst.close();
            } catch (Exception e) {
            }
        }

        try {
            //Wyszukiwarka po loginie użytkownika
            String sql = "select * from USERS where USERID=? ";
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_search.getText());

            rs = pst.executeQuery();
            if (rs.next()) {
                String add1 = rs.getString("ID");
                txt_employeeid.setText(add1);
                String add2 = rs.getString("Surname");
                txt_surname.setText(add2);
                String add3 = rs.getString("Name");
                txt_name.setText(add3);
                
                //zmienna pomocnicza - Sprawdzamy poziom dostępu i ustawiamy index w liście rozwijanej
                String add4temp = rs.getString("Access_level");
                Integer add4 = 0;
                if ("Admin".equals(add4temp)) {
                    add4 = 0;
                } else if ("Moderator".equals(add4temp)) {
                    add4 = 1;

                } else {
                    add4 = 2;
                }
                txt_access.setSelectedIndex(add4);

                String add9 = rs.getString("Userid");
                txt_userid.setText(add9);
                String add10 = rs.getString("password");
                txt_password.setText(add10);

            }
        } catch (Exception e) {

        } finally {
            try {
                rs.close();
                pst.close();
                //conn.close();
            } catch (Exception e) {
            }
        }


    }//GEN-LAST:event_txt_searchKeyReleased

    private void jTable1_pumpdataMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1_pumpdataMouseClicked

        //Pobieramy dane z tabeli i uzupełniamy pola
        int row = jTable1_pumpdata.getSelectedRow();
        txt_employeeid.setText(jTable1_pumpdata.getModel().getValueAt(row, 0).toString());
        txt_surname.setText(jTable1_pumpdata.getModel().getValueAt(row, 1).toString());
        txt_name.setText(jTable1_pumpdata.getModel().getValueAt(row, 2).toString());
        String add4temp = jTable1_pumpdata.getModel().getValueAt(row, 5).toString();

        //Sprawdzamy poziom dostępu i ustawiamy index w liście rozwijanej 
        Integer add4 = 0;
        if ("Admin".equals(add4temp)) {
            add4 = 0;
        } else if ("Moderator".equals(add4temp)) {
            add4 = 1;

        } else {
            add4 = 2;
        }
        txt_access.setSelectedIndex(add4);

        txt_userid.setText(jTable1_pumpdata.getModel().getValueAt(row, 3).toString());
        txt_password.setText(jTable1_pumpdata.getModel().getValueAt(row, 4).toString());


    }//GEN-LAST:event_jTable1_pumpdataMouseClicked

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // Otwieramy zakładke O nas
        About_us p = new About_us();
        p.setVisible(true);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void cmd_SignOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmd_SignOutActionPerformed
        // Wyloguj się
        try {
            rs.close();
            pst.close();
            conn.close();
        } catch (Exception e) {
        }
        Login_Frame s = new Login_Frame();
        s.setVisible(true);
        close3();
    }//GEN-LAST:event_cmd_SignOutActionPerformed

    private void txt_employeeidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_employeeidActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_employeeidActionPerformed

    private void txt_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_nameActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // Otwieramy "Kupione"
        try {
            rs.close();
            pst.close();
            conn.close();
        } catch (Exception e) {
        }

        Lombard s = new Lombard();
        s.setVisible(true);

        close3();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void cmd_refreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmd_refreshActionPerformed
        //Aktualizujemy tabele
        UpdateJTable();
    }//GEN-LAST:event_cmd_refreshActionPerformed

    private void cmd_clearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmd_clearActionPerformed
        // Czyscimy pola wejścia
        txt_employeeid.setText("");
        txt_name.setText("");
        txt_surname.setText("");
        txt_userid.setText("");
        txt_password.setText("");
        txt_access.setSelectedIndex(0);

    }//GEN-LAST:event_cmd_clearActionPerformed

    private void cmd_deletaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmd_deletaActionPerformed
        //Usuwamy użytkownika
        String sql = "delete from USERS where ID=?";
        try {
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_employeeid.getText());
            pst.execute();

            JOptionPane.showMessageDialog(null, "Usunięto");
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                rs.close();
                pst.close();
            } catch (Exception e) {
            }
        }
    }//GEN-LAST:event_cmd_deletaActionPerformed

    private void cmd_editActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmd_editActionPerformed
        // Aktualizacja użytkownika
        try {
            String value1 = txt_employeeid.getText();
            String value2 = txt_name.getText();
            String value3 = txt_surname.getText();
            String value4 = txt_userid.getText();
            String value5 = txt_password.getText();
            Integer value6 = txt_access.getSelectedIndex();

            String sql = "update USERS set name='" + value2 + "', surname='" + value3 + "',userid='" + value4 + "',password='" + value5 + "', Access_level='" + value6 + "' where ID='" + value1 + "'";
            pst = conn.prepareStatement(sql);
            pst.execute();
            JOptionPane.showMessageDialog(null, "Zaaktualizowano");
           
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                rs.close();
                pst.close();
            } catch (Exception e) {
            }
        }
    }//GEN-LAST:event_cmd_editActionPerformed

    private void cmd_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmd_addActionPerformed
        //Dodajemy nowego użytkownika
        String sql = "INSERT INTO USERS (ID,name,surname,Userid,password,Access_level) values(?,?,?,?,?,?)";
        try {
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_employeeid.getText());
            pst.setString(2, txt_name.getText());
            pst.setString(3, txt_surname.getText());
            pst.setString(4, txt_userid.getText());
            pst.setString(5, txt_password.getText());
            pst.setInt(6, txt_access.getSelectedIndex());

            pst.execute();

            JOptionPane.showMessageDialog(null, "Saved");

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                rs.close();
                pst.close();
            } catch (Exception e) {
            }
        }
    }//GEN-LAST:event_cmd_addActionPerformed

    private void txt_accessActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_accessActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_accessActionPerformed

  /** Klasa main panelu edycji danych użytkownika
     * @param args*/
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Admin_JFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Admin_JFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Admin_JFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Admin_JFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Utworz okno panelu edycji użytkowników*/
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new Admin_JFrame().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu Lbl_time;
    private javax.swing.JButton cmd_SignOut;
    private javax.swing.JButton cmd_add;
    private javax.swing.JButton cmd_clear;
    private javax.swing.JButton cmd_deleta;
    private javax.swing.JButton cmd_edit;
    private javax.swing.JButton cmd_refresh;
    private javax.swing.JButton cmd_search;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JTable jTable1_pumpdata;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JComboBox txt_access;
    private javax.swing.JTextField txt_employeeid;
    private javax.swing.JTextField txt_name;
    private javax.swing.JTextField txt_password;
    private javax.swing.JTextField txt_search;
    private javax.swing.JTextField txt_surname;
    private javax.swing.JTextField txt_userid;
    // End of variables declaration//GEN-END:variables
}
